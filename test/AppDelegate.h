//
//  AppDelegate.h
//  test
//
//  Created by WangMenglan on 21/5/2016.
//  Copyright © 2016 WangMenglan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

