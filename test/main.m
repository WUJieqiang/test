//
//  main.m
//  test
//
//  Created by WangMenglan on 21/5/2016.
//  Copyright © 2016 WangMenglan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
